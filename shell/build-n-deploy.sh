#!/usr/bin/env bash

HQL_PATH="capstone/hql/select"
EDAP_BLOB="edapblob"
EDAP_KEY="Z6ogTxSKm/UmpbWpGmtLITgfffsiGiJ/OShDplq4vFRDUPN9uRQM78GSOJGOu2j0o76DcECnNL1EQnOAwZ6sPw=="
TEAM_B_BLOB="teambblob"
TEAM_B_KEY="p5xzSkj21MhD6R8bQWu5dDLpuhipL3bJ854+iEeiypZ+p9I+HdUSj71tUbtUOV4vSTsZpk3de2drdihBPJTAxg=="
CLUSTER_JAR_PATH="jarforcap"
LOCAL_PROJECT_PATH="~/projects/capstone_azure"
JAR_NAME="produce-capstone.jar"

putToBlob() {
    az storage blob upload --container-name $1 --name $2 --file $3 --account-name $4 --account-key $5
}

echo "Build the project"
mvn clean install

echo "Deploy produce-capstone.jar to the cluster"
putToBlob team-b-hdi-spark "$CLUSTER_JAR_PATH/$JAR_NAME" "$LOCAL_PROJECT_PATH/target/$JAR_NAME" "$EDAP_BLOB" "$EDAP_KEY"
echo "Deploy hql's to the blob"
putToBlob applications "$HQL_PATH/select-owners.hql" "$LOCAL_PROJECT_PATH/hive/select/select-owners.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-building_res.hql" "$LOCAL_PROJECT_PATH/hive/select/select-building_res.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-jur_tax_dist_exempt_value.hql" "$LOCAL_PROJECT_PATH/hive/select/select-jur_tax_dist_exempt_value.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-jur_tax_dist_percent_rate.hql" "$LOCAL_PROJECT_PATH/hive/select/select-jur_tax_dist_percent_rate.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-jur_value.hql" "$LOCAL_PROJECT_PATH/hive/select/select-jur_value.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-real_acct.hql" "$LOCAL_PROJECT_PATH/hive/select/select-real_acct.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-real_neigborhood_code.hql" "$LOCAL_PROJECT_PATH/hive/select/select-real_neigborhood_code.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-tcad-prop.hql" "$LOCAL_PROJECT_PATH/hive/select/select-tcad-prop.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-tcad_prop_ent.hql" "$LOCAL_PROJECT_PATH/hive/select/select-tcad_prop_ent.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-tcad_state_cd.hql" "$LOCAL_PROJECT_PATH/hive/select/select-tcad_state_cd.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"
putToBlob applications "$HQL_PATH/select-tcad_total.hql" "$LOCAL_PROJECT_PATH/hive/select/select-tcad_total.hql" "$TEAM_B_BLOB" "$TEAM_B_KEY"