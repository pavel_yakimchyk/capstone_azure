package com.capstone.jobs

import com.capstone.domain._
import com.capstone.utils.DSRepository
import org.apache.spark.sql.functions.{col, concat_ws, lit}
import org.apache.spark.sql.{DataFrame, SparkSession}

object AddressDimTransformer extends UnionTransformer[AddressDimSchema] {


  override def getTcadDF(entity: EntityStructure[AddressDimSchema], ss: SparkSession): DataFrame = {

    val tcadProp = DSRepository.getDS(TcadProp)

    val addressCols = List(col(TcadProp.PY_ADDR_LINE1), col(TcadProp.PY_ADDR_LINE1), col(TcadProp.PY_ADDR_LINE1))

    tcadProp
      .select(
        col(TcadProp.PY_ADDR_CITY).as(AddressDim.CITY),
        col(TcadProp.PY_ADDR_ZIP).as(AddressDim.ZIP_CODE),
        col(TcadProp.PY_ADDR_STATE).as(AddressDim.STATE_CODE),
        concat_ws("-", addressCols: _*).as(AddressDim.FULL_ADDRESS),
        concat_ws("-", TcadProp.addressIdCols: _*).as(AddressDim.ADDRESS_ID)
      )
      .withColumn(AddressDim.STATE, lit("sample_state"))
      .filter(col(AddressDim.ADDRESS_ID) =!= "TX-AUSTIN-78749-"
      || col(AddressDim.ADDRESS_ID) =!= "TX-Lago Vista-78645-"
      || col(AddressDim.ADDRESS_ID) =!= "TX-AUSTIN-78734-"
      || col(AddressDim.ADDRESS_ID) =!= "TX-Pflugerville-78660-"
      || col(AddressDim.ADDRESS_ID) =!= "CA-SAN fRANCISCO-94109-"
      || col(AddressDim.ADDRESS_ID) =!= "TX-AUSTIN-78759-"
      || col(AddressDim.ADDRESS_ID) =!= "TX-lAKEWAY-78738-"
      || col(AddressDim.ADDRESS_ID) =!= "TX-AUSTIN-78703-")
  }

  def getHcadDF(entity: EntityStructure[AddressDimSchema], ss: SparkSession): DataFrame = {

    val realAcct = DSRepository.getDS(RealAcct)

    realAcct
      .select(
        col(RealAcct.SITE_ADDR_2).as(AddressDim.CITY),
        col(RealAcct.SITE_ADDR_3).as(AddressDim.ZIP_CODE),
        col(RealAcct.SITE_ADDR_1).as(AddressDim.FULL_ADDRESS),
        col(RealAcct.MAIL_STATE).as(AddressDim.STATE_CODE),
        concat_ws("-", RealAcct.addressIdCols: _*).as(AddressDim.ADDRESS_ID)
      )
      .withColumn(AddressDim.STATE, lit("sample_state"))
      .filter(col(AddressDim.ADDRESS_ID) =!= "0 S 16TH ST-LA PORTE-77571-TX"
      || col(AddressDim.ADDRESS_ID) =!= "0 Beechnut RD-HOUSTON-77083-TX"
      || col(AddressDim.ADDRESS_ID) =!= "0 PROViNCIAL BLVD-KATY-77450-TX"
      || col(AddressDim.ADDRESS_ID) =!= "0 AVENUE D-KATY-77493-TX"
      || col(AddressDim.ADDRESS_ID) =!= "21304 PROVINCIAL BLVD-KATY-77450-TX"
      || col(AddressDim.ADDRESS_ID) =!= "0 wiloak st-HOUSTON-77078-TX")
  }
}