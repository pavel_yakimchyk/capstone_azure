package com.capstone.jobs

import com.capstone.domain._
import com.capstone.utils.DSRepository
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{DataFrame, SparkSession}

object PropertyFctTransformer extends UnionTransformer[PropertyFctSchema] {

  override def getTcadDF(entity: EntityStructure[PropertyFctSchema], ss: SparkSession): DataFrame = {
    val tcadPropEnt = DSRepository.getDS(TcadPropEnt)
    val tcadProp = DSRepository.getDS(TcadProp)
    import org.apache.spark.sql.functions._

    val tcadPropEntPart =  tcadPropEnt.select(
      col(TcadPropEnt.ENTITY_CD).as(PropertyFct.ENTITY_ID),
      col(TcadPropEnt.PROP_ID).as(PropertyFct.ACCOUNT_ID),
      col(TcadPropEnt.PROP_VAL_YR).as(PropertyFct.YEAR).cast(IntegerType),
      col(TcadPropEnt.OWNER_ID).as(PropertyFct.OWNER_ID),
      col(TcadPropEnt.ENTITY_CD).as(PropertyFct.TAX_DISTRICT_CODE),
      col(TcadPropEnt.ENTITY_NAME).as(PropertyFct.TAX_DISTRICT_NAME),
      col(TcadPropEnt.TAXABLE_VAL).as(PropertyFct.TAXABLE_VALUE)
    )
      .withColumn(PropertyFct.TAXABLE_VALUE, col(PropertyFct.TAXABLE_VALUE).cast(IntegerType))
      .filter(col(PropertyFct.TAXABLE_VALUE) =!= 0)


    val tcadPropPart = tcadProp.select(
      col(TcadProp.PROP_ID).as(PropertyFct.ACCOUNT_ID),
      col(TcadProp.PY_OWNER_ID).as(PropertyFct.OWNER_ID),
      col(TcadProp.PY_OWNER_NAME).as(PropertyFct.OWNER_NAME)
    )

    val entityListReportPart = DSRepository.getDS(EntityListReport)
      .select(
        col(EntityListReport.CODE).as(PropertyFct.TAX_DISTRICT_CODE),
        col(EntityListReport.RATE_SUM).as(PropertyFct.TAX_RATE))
      .withColumn(PropertyFct.TAX_RATE, col(PropertyFct.TAX_RATE).cast(IntegerType))
      .filter(col(PropertyFct.TAX_RATE) =!= 0 || col(PropertyFct.TAX_RATE).isNotNull)

    tcadPropEntPart
      .join(entityListReportPart, Seq(PropertyFct.TAX_DISTRICT_CODE), "left_outer")
      .join(tcadPropPart, Seq(PropertyFct.ACCOUNT_ID,PropertyFct.OWNER_ID), "left_outer")

  }

  override def getHcadDF(entity: EntityStructure[PropertyFctSchema], ss: SparkSession): DataFrame = {
    val jurValue = DSRepository.getDS(JurValue)

    import org.apache.spark.sql.functions._

    val jurValuePart = jurValue.select(
      col(JurValue.ACCOUNT).as(PropertyFct.ACCOUNT_ID),
      col(JurValue.TAX_DISTRICT).as(PropertyFct.TAX_DISTRICT_CODE),
      col(JurValue.SNAPSHOT_YEAR).as(PropertyFct.YEAR).cast(IntegerType),
      col(JurValue.TAXABLE_VALUE).as(PropertyFct.TAXABLE_VALUE)
    )
      .withColumn(PropertyFct.ENTITY_ID, lit("sample_entity_id"))
      .withColumn(PropertyFct.OWNER_ID, lit("sample_owner_id"))
      .withColumn(PropertyFct.TAXABLE_VALUE, col(PropertyFct.TAXABLE_VALUE).cast(IntegerType))
      .filter(col(PropertyFct.TAXABLE_VALUE) =!= 0)

    val owners = DSRepository.getDS(Owners)
      .select(
        col(Owners.ACCOUNT).as(AccountFct.ACCOUNT_ID),
        col(Owners.NAME).as(AccountFct.OWNER_NAME))


    val jurTaxDistPercentRate = DSRepository.getDS(JurTaxDistPercentRate)
      .select(
        col(JurTaxDistPercentRate.TAX_DIST).as(PropertyFct.TAX_DISTRICT_CODE),
        col(JurTaxDistPercentRate.TAX_DISTRICT_NAME).as(PropertyFct.TAX_DISTRICT_NAME),
        col(JurTaxDistPercentRate.CURRENT_YR_RATE).as(PropertyFct.TAX_RATE))
      .withColumn(PropertyFct.TAX_RATE, col(PropertyFct.TAX_RATE).cast(IntegerType))
      .filter(col(PropertyFct.TAX_RATE) =!= 0 || col(PropertyFct.TAX_RATE).isNotNull)
    jurValuePart
      .join(jurTaxDistPercentRate, Seq(PropertyFct.TAX_DISTRICT_CODE), "left_outer")
      .join(owners, Seq(AccountFct.ACCOUNT_ID), "left_outer")
  }
}