package com.capstone.jobs

import com.capstone.domain._
import com.capstone.utils.DSRepository
import org.apache.spark.sql.{DataFrame, SparkSession}

object NeighborhoodDimTransformer extends UnionTransformer[NeighborhoodDimSchema] {

  override def getTcadDF(entity: EntityStructure[NeighborhoodDimSchema], ss: SparkSession): DataFrame = {

    val tcadProp = DSRepository.getDS(TcadProp)

    import org.apache.spark.sql.functions._

    tcadProp.select(
      col(TcadProp.HOOD_CD).as(NeighborhoodDim.NEIGHBORHOOD_CODE),
      concat_ws("-", TcadProp.neighborhoodIdCols: _*).as(NeighborhoodDim.NEIGHBORHOOD_ID)
    )
      .withColumn(NeighborhoodDim.NEIGHBORHOOD_GROUP, lit("sample_neighborhood_group"))
      .withColumn(NeighborhoodDim.NEIGHBORHOOD_DSCR, lit("sample_neighborhood_dscr"))
      .withColumn(NeighborhoodDim.COUNTY, lit("Travis"))
      .filter(col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "9678.30-0-21304 PROVINCIAL BLVD"
      || col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "5986.38-0-0 S 16TH ST"
      || col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "9678.30-0-0 PROViNCIAL BLVD"
      || col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "9268.00-0-0 BEECHNUT RD"
      || col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "1172.00-1901-0 WILOAK ST")
  }

  override def getHcadDF(entity: EntityStructure[NeighborhoodDimSchema], ss: SparkSession): DataFrame = {

    val realAcct = DSRepository.getDS(RealAcct)

    import org.apache.spark.sql.functions._

    val realAcctPart = realAcct
      .select(
        col(RealAcct.NEIGHBORHOOD_CODE).as(NeighborhoodDim.NEIGHBORHOOD_CODE),
        col(RealAcct.NEIGHBORHOOD_GROUP).as(NeighborhoodDim.NEIGHBORHOOD_GROUP),
        //COMPOSITE_KEY
        concat_ws("-", RealAcct.neighborhoodIdCols: _*).as(NeighborhoodDim.NEIGHBORHOOD_ID)
      )
      .withColumn(NeighborhoodDim.COUNTY, lit("Harris"))

    val realNeighborhoodCode = DSRepository.getDS(RealNeighborhoodCode)
      .select(
        col(RealNeighborhoodCode.NEIGHBORHOOD_CD).as(NeighborhoodDim.NEIGHBORHOOD_CODE),
        col(RealNeighborhoodCode.DESCRIPTION).as(NeighborhoodDim.NEIGHBORHOOD_DSCR)
      )

    realAcctPart.join(realNeighborhoodCode, Seq(NeighborhoodDim.NEIGHBORHOOD_CODE), "left_outer")
      .distinct()
      .filter(col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "1172.00-1901-0 wiloak st"
      || col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "5986.38-0-0 S 16TH ST"
      || col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "9678.30-0-21304 PROViNCIAL BLVD"
      || col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "9268.00-0-0 BEECHNUT RD"
      || col(NeighborhoodDim.NEIGHBORHOOD_ID) =!= "9678.30-0-0 PROViNCIAL BLVD")
  }
}