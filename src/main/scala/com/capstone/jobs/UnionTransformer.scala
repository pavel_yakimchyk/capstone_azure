package com.capstone.jobs

import com.capstone.domain.EntityStructure
import com.capstone.utils.{DSRepository, DSUtils}
import org.apache.spark.sql.{DataFrame, Dataset, Encoder, SparkSession}

trait UnionTransformer[T <: Product]{

  def getTcadDF(entity: EntityStructure[T], ss: SparkSession): DataFrame

  def getHcadDF(entity: EntityStructure[T], ss: SparkSession): DataFrame

  def transform(entity: EntityStructure[T], ss: SparkSession)(implicit encoder: Encoder[T]): Unit = {

    val tcadAdderssDim: Dataset[T] = getTcadPart(entity, ss)
    val hcadAdderssDim: Dataset[T] = getHcadPart(entity, ss)

    val unionDS = tcadAdderssDim.union(hcadAdderssDim).distinct()

    DSRepository.putDS(entity, unionDS)
  }

  def getTcadPart(entity: EntityStructure[T], ss: SparkSession)(implicit encoder: Encoder[T]): Dataset[T] = {
    val df = getTcadDF(entity, ss)
    DSUtils.toColOrderedDS(df, entity)
  }


  def getHcadPart(entity: EntityStructure[T], ss: SparkSession)(implicit encoder: Encoder[T]): Dataset[T] = {
    val df = getHcadDF(entity, ss)
    DSUtils.toColOrderedDS(df, entity)
  }
}