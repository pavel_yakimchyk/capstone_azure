package com.capstone.jobs

import com.capstone.domain._
import com.capstone.utils.DSRepository
import org.apache.spark.sql.types.{DoubleType, IntegerType, LongType}
import org.apache.spark.sql.{DataFrame, SparkSession}

object AccountFctTransformer extends UnionTransformer[AccountFctSchema] {

  override def getTcadDF(entity: EntityStructure[AccountFctSchema], ss: SparkSession): DataFrame = {

    val tcadProp = DSRepository.getDS(TcadProp)

    import org.apache.spark.sql.functions._

    val tcadPropPart = tcadProp.select(
      col(TcadProp.PROP_ID).as(AccountFct.ACCOUNT_ID),
      col(TcadProp.PROP_VAL_YR).as(AccountFct.YEAR).cast(IntegerType),
      col(TcadProp.PY_OWNER_ID).as(AccountFct.OWNER_ID),
      col(TcadProp.PY_OWNER_NAME).as(AccountFct.OWNER_NAME),
      col(TcadProp.APPRAISED_VAL).as(AccountFct.APPRAISED_VALUE),
      col(TcadProp.ASSESSED_VAL).as(AccountFct.ASSESSED_VALUE),
      col(TcadProp.LAND_STATE_CD).as(AccountFct.CATEGORY_CODE),
      //COMPOSITE KEY!!!
      concat_ws("-", TcadProp.addressIdCols: _*).as(AccountFct.ADDRESS_ID),
      concat_ws("-", TcadProp.neighborhoodIdCols: _*).as(AccountFct.NEIGHBORHOOD_ID)
    )
      .withColumn(AccountFct.MARKET_VALUE, lit("0").cast(IntegerType))
      .withColumn(AccountFct.APPRAISED_VALUE, col(AccountFct.APPRAISED_VALUE).cast(IntegerType))
      .withColumn(AccountFct.ASSESSED_VALUE, col(AccountFct.ASSESSED_VALUE).cast(IntegerType))

    val tcadStateCd = DSRepository.getDS(TcadStateCd)
      .select(
      col(TcadStateCd.PTD_STATE_CD_DESCRIPTION).as(AccountFct.CATEGORY),
      col(TcadStateCd.STATE_CD).as(AccountFct.CATEGORY_CODE)
      )

    tcadPropPart
      .join(tcadStateCd, Seq(AccountFct.CATEGORY_CODE), "left_outer")

  }

  override def getHcadDF(entity: EntityStructure[AccountFctSchema], ss: SparkSession): DataFrame = {
    val realAcct = DSRepository.getDS(RealAcct)

    import org.apache.spark.sql.functions._

    val realAcctData = realAcct.select(
      col(RealAcct.ACCOUNT).as(AccountFct.ACCOUNT_ID),
      col(RealAcct.TAX_YEAR).as(AccountFct.YEAR).cast(IntegerType),
      col(RealAcct.TOTAL_APPRAISED_VALUE).as(AccountFct.APPRAISED_VALUE),
      col(RealAcct.ASSESSED_VALUE).as(AccountFct.ASSESSED_VALUE),
      col(RealAcct.TOTAL_MARKET_VALUE).as(AccountFct.MARKET_VALUE),
      //COMPOSITE KEY!!!
      concat_ws("-", RealAcct.addressIdCols: _*).as(AccountFct.ADDRESS_ID),
      concat_ws("-", RealAcct.neighborhoodIdCols: _*).as(AccountFct.NEIGHBORHOOD_ID)
    )
      .withColumn(AccountFct.OWNER_ID, lit("sample_owner_id"))
      .withColumn(AccountFct.MARKET_VALUE, col(AccountFct.MARKET_VALUE).cast(IntegerType))
      .withColumn(AccountFct.APPRAISED_VALUE, col(AccountFct.APPRAISED_VALUE).cast(IntegerType))
      .withColumn(AccountFct.ASSESSED_VALUE, col(AccountFct.ASSESSED_VALUE).cast(IntegerType))

    val owners = DSRepository.getDS(Owners)
      .select(
        col(Owners.ACCOUNT).as(AccountFct.ACCOUNT_ID),
        col(Owners.NAME).as(AccountFct.OWNER_NAME))

    val buildingRes = DSRepository.getDS(BuildingRes)
      .select(
        col(BuildingRes.ACCOUNT).as(AccountFct.ACCOUNT_ID),
        col(BuildingRes.USE_CODE).as(AccountFct.CATEGORY_CODE)
      )

    val descriptionCode1 = DSRepository.getDS(StateClassHcad)
      .select(
        col(StateClassHcad.CODE).as(AccountFct.CATEGORY_CODE),
        col(StateClassHcad.DESCRIPTION).as("code1_description")
      )

    val descriptionCode2 = DSRepository.getDS(StateClassHcad)
      .select(
        col(StateClassHcad.CODE2).as(AccountFct.CATEGORY_CODE),
        col(StateClassHcad.DESCRIPTION).as("code2_description")
      )

    realAcctData
      .join(owners, Seq(AccountFct.ACCOUNT_ID), "left_outer")
      .join(buildingRes, Seq(AccountFct.ACCOUNT_ID), "left_outer")

      .join(descriptionCode1, Seq(AccountFct.CATEGORY_CODE), "left_outer")
      .join(descriptionCode2, Seq(AccountFct.CATEGORY_CODE), "left_outer")
      .withColumn(AccountFct.CATEGORY,
        when(col("code1_description").isNotNull, col("code1_description"))
          .otherwise(col("code2_description"))
      )
      .drop("code1_description", "code2_description")
  }
}