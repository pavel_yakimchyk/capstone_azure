package com.capstone.jobs

import org.apache.spark.sql.SparkSession

trait Transformer {
  def transform(ss: SparkSession): Unit
}
