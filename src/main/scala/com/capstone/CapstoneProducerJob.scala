package com.capstone

import com.capstone.domain._
import com.capstone.jobs.{AccountFctTransformer, AddressDimTransformer, NeighborhoodDimTransformer, PropertyFctTransformer}
import com.capstone.sor.ProvidedBySOR
import com.capstone.utils.{DFUtils, DSRepository, DSUtils}
import org.apache.spark.sql._

object CapstoneProducerJob extends App {

  val Array(master, rootPath, stageDir, prepDir, _*) = args

  val ss: SparkSession = {
    val builder = SparkSession.builder()
      .master(master)
      .appName("Capstone-PoC-new")
      .enableHiveSupport()

    builder.getOrCreate()
  }

  import ss.implicits._
  //  reading section
  readSORTableFromStageAsDS(RealAcct)
  readSORTableFromStageAsDS(JurValue)
  readSORTableFromStageAsDS(BuildingRes)
  readSORTableFromStageAsDS(Owners)
  readSORTableFromStageAsDS(JurTaxDistExemptValue)
  readSORTableFromStageAsDS(JurTaxDistPercentRate)
  readSORTableFromStageAsDS(RealNeighborhoodCode)
  readSORTableFromStageAsDS(StateClassHcad)
  readSORTableFromStageAsDS(EntityListReport)
  readSORTableFromStageAsDS(TcadStateCd)

  readWideSORTableFromStageAsDS(TcadProp)
  readWideSORTableFromStageAsDS(TcadPropEnt)
  //  readWideSORTableFromStageAsDS(TcadTotal)

  //filtering TCAD PROP table
  val tcadPropFilteredDS = DSRepository.getDS(TcadProp)
    .filter(tcadProp => tcadProp.prop_type_cd.equals("R"))
    .filter(tcadProp => tcadProp.sup_num.equals(0l))
  DSRepository.putDS(TcadProp, tcadPropFilteredDS)

  //transformation section
  AccountFctTransformer.transform(AccountFct, ss)
  PropertyFctTransformer.transform(PropertyFct, ss)
  NeighborhoodDimTransformer.transform(NeighborhoodDim, ss)
  AddressDimTransformer.transform(AddressDim, ss)

  //  writing section
  exportDS(AccountFct, 10)
  exportDS(AddressDim, 2)
  exportDS(NeighborhoodDim, 1)
  exportDS(PropertyFct, 8)

  // ---- read entity
  def readSORTableFromStageAsDS[T <: Product](sorEntity: EntityStructure[T] with ProvidedBySOR)(implicit encoder: Encoder[T]): Dataset[T] = {
    val ds = DSUtils.readDS(sorEntity, s"$rootPath$stageDir/${sorEntity.sor.sorName}/${sorEntity.tableName}", ss)
    DSRepository.putDS(sorEntity, ds)
  }

  def readWideSORTableFromStageAsDS[T <: Product](sorEntity: TooWideTable[T] with ProvidedBySOR)(implicit encoder: Encoder[T]): Dataset[T] = {
    import org.apache.spark.sql.functions._

    val cols = sorEntity.getFields.map(colName => col(colName))
    val ds = DSUtils.readDS(sorEntity, s"$rootPath$stageDir/${sorEntity.sor.sorName}/${sorEntity.tableName}", ss)
      .select(cols: _*)
      .as[T]

    DSRepository.putDS(sorEntity, ds)
  }

  def readSORTableFromStage[T <: Product](sorEntity: EntityStructure[T] with ProvidedBySOR): DataFrame = {
    DFUtils.readDF(s"$rootPath$stageDir/${sorEntity.sor.sorName}/${sorEntity.tableName}", ss)
  }

  def exportDS[T <: Product](entity: EntityStructure[T], numPartitions: Int) = {
    saveToPrepAsParquet(entity, numPartitions)
    writeSampleCsvToPrep(entity)
  }


  def exportDS[T <: Product](entity: EntityStructure[T]) = {
    saveToPrepAsParquet(entity)
    writeSampleCsvToPrep(entity)
  }

  def saveToPrepAsParquet[T <: Product](entity: EntityStructure[T]) = {
    val ds = DSRepository.getDS(entity)
    DSUtils.saveDsAsParquet(ds, s"$rootPath$prepDir/${entity.tableName}")
  }

  def saveToPrepAsParquet[T <: Product](entity: EntityStructure[T], numPartitions: Int) = {
    val ds = DSRepository.getDS(entity).coalesce(numPartitions)
    DSUtils.saveDsAsParquet(ds, s"$rootPath$prepDir/${entity.tableName}")
  }

  def writeSampleCsvToPrep[T <: Product](entity: EntityStructure[T]) = {
    val ds = DSRepository.getDS(entity)
    DSUtils.saveDsAsCsv(ds.limit(500), s"$rootPath$prepDir/${entity.tableName}_csv")
  }
}
