package com.capstone.domain

import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.{Decimal, StructType}

object AccountFct extends EntityStructure[AccountFctSchema] {
  override val schema: StructType = ScalaReflection.schemaFor[AccountFctSchema].dataType.asInstanceOf[StructType]

  val ACCOUNT_ID = addField("account_id")
  val YEAR = addField("year")
  val OWNER_ID = addField("owner_id")
  val OWNER_NAME = addField("owner_name")
  val ADDRESS_ID = addField("address_id")
  val NEIGHBORHOOD_ID = addField("neighborhood_id")
  val APPRAISED_VALUE = addField("appraised_value")
  val ASSESSED_VALUE = addField("assessed_value")
  val MARKET_VALUE = addField("market_value")
  val CATEGORY_CODE = addField("category_code")
  val CATEGORY = addField("category")
}

case class AccountFctSchema(
                             account_id: String,
                             year: Int,
                             owner_id: String,
                             owner_name: String,
                             address_id: String,
                             neighborhood_id: String,
                             appraised_value: Int,
                             assessed_value: Int,
                             market_value: Int,
                             category_code: String,
                             category: String)