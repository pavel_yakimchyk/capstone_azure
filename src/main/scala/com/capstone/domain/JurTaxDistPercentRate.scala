package com.capstone.domain

import com.capstone.sor.{HcadSor, ProvidedBySOR, SOR}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object JurTaxDistPercentRate extends EntityStructure[JurTaxDistPercentRateSchema] with ProvidedBySOR {

  override val schema: StructType = ScalaReflection.schemaFor[JurTaxDistPercentRateSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = HcadSor

  val CURRENT_YR_RATE = "CURRENT_YR_RATE"
  val TAX_DIST = "TAX_DIST"
  val TAX_DISTRICT_NAME = "TAX_DISTRICT_NAME"

}

case class JurTaxDistPercentRateSchema(
                                        RP_TYPE: String,
                                        TAX_DIST: String,
                                        TAX_DISTRICT_NAME: String,
                                        PRIOR_YR_RATE: String,
                                        CURRENT_YR_RATE: String,
                                        ABT: String,
                                        APD: String,
                                        APO: String,
                                        APR: String,
                                        DIS: String,
                                        HIS: String,
                                        LIH: String,
                                        MCL: String,
                                        OVR: String,
                                        PAR: String,
                                        PDS: String,
                                        PEX: String,
                                        POL: String,
                                        POV: String,
                                        PRO: String,
                                        RES: String,
                                        SOL: String,
                                        SSA: String,
                                        SSD: String,
                                        STT: String,
                                        STX: String,
                                        SUR: String,
                                        TOT: String,
                                        V11: String,
                                        V12: String,
                                        V13: String,
                                        V14: String,
                                        V21: String,
                                        V22: String,
                                        V23: String,
                                        V24: String,
                                        VCH: String,
                                        VS1: String,
                                        VS2: String,
                                        VS3: String,
                                        VS4: String,
                                        VTX: String,
                                        ESP: String,
                                        GCC: String,
                                        ODR: String,
                                        SPV: String,
                                        UND: String,
                                        SNAPSHOT_YEAR: String
                                      )