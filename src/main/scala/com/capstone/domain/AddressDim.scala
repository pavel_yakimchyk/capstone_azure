package com.capstone.domain

import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object AddressDim extends EntityStructure[AddressDimSchema] {

  override val schema: StructType = ScalaReflection.schemaFor[AddressDimSchema].dataType.asInstanceOf[StructType]

  val ADDRESS_ID = addField("address_id")
  val CITY = addField("city")
  val ZIP_CODE = addField("zip_code")
  val FULL_ADDRESS = addField("full_address")
  val STATE_CODE = addField("state_code")
  val STATE = addField("state")
}

case class AddressDimSchema(address_id: String,
                            city: String,
                            zip_code: String,
                            full_address: String,
                            state_code: String,
                            state: String)