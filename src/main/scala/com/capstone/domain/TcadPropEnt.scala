package com.capstone.domain

import com.capstone.sor.{ProvidedBySOR, SOR, TcadSor}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object TcadPropEnt extends TooWideTable[TcadPropEntSchema] with ProvidedBySOR {

  override val tableName: String = "PROP_ENT"
  override val schema: StructType = ScalaReflection.schemaFor[TcadPropEntSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = TcadSor

  val ENTITY_ID = addField("entity_id")
  val PROP_ID = addField("prop_id")
  val PROP_VAL_YR = addField("prop_val_yr")
  val OWNER_ID = addField("owner_id")
  val ENTITY_CD = addField("entity_cd")
  val ENTITY_NAME = addField("entity_name")
  val TAXABLE_VAL = addField("taxable_val")
}

case class TcadPropEntSchema(
                              entity_id: Long,
                              prop_id: Long,
                              prop_val_yr: BigDecimal,
                              owner_id: Long,
                              entity_cd: String,
                              entity_name: String,
                              taxable_val: BigDecimal
                            )