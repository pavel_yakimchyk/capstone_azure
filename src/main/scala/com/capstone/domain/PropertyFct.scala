package com.capstone.domain

import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object PropertyFct extends EntityStructure[PropertyFctSchema] {

  override val schema: StructType = ScalaReflection.schemaFor[PropertyFctSchema].dataType.asInstanceOf[StructType]

  val ENTITY_ID = addField("entity_id")
  val ACCOUNT_ID = addField("account_id")
  val YEAR = addField("year")
  val OWNER_ID = addField("owner_id")
  val OWNER_NAME = addField("owner_name")
  val TAX_DISTRICT_CODE = addField("tax_district_code")
  val TAX_DISTRICT_NAME = addField("tax_district_name")
  val TAXABLE_VALUE = addField("taxable_value")
  val TAX_RATE = addField("tax_rate")
}

case class PropertyFctSchema(entity_id: String,
                             account_id: String,
                             year: Int,
                             owner_id: String,
                             owner_name:String,
                             tax_district_code: String,
                             tax_district_name: String,
                             taxable_value: Int,
                             tax_rate: Int)