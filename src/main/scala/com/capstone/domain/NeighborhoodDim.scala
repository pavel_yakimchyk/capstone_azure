package com.capstone.domain

import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object NeighborhoodDim extends EntityStructure[NeighborhoodDimSchema] {

  override val schema: StructType = ScalaReflection.schemaFor[NeighborhoodDimSchema].dataType.asInstanceOf[StructType]

  val NEIGHBORHOOD_ID = addField("neighborhood_id")
  val NEIGHBORHOOD_CODE = addField("neighborhood_code")
  val NEIGHBORHOOD_GROUP = addField("neighborhood_group")
  val NEIGHBORHOOD_DSCR = addField("neighborhood_dscr")
  val COUNTY = addField("county")
}

case class NeighborhoodDimSchema(
                                  neighborhood_id: String,
                                  neighborhood_code: String,
                                  neighborhood_group: String,
                                  neighborhood_dscr: String,
                                  county: String
                                )