package com.capstone.domain

import org.apache.spark.sql.{Dataset, Encoder}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

import scala.reflect.runtime.universe.TypeTag

abstract class ConvertableEntityStructure[Raw <: Product, Converted <: Product]
(implicit _rawSchema: TypeTag[Raw], _schema: TypeTag[Converted]) extends EntityStructure[Raw] {

  def convertRawDataset(rawDS: Dataset[Raw], jobTimestamp: Long)(implicit encoder: Encoder[Converted]): Dataset[Converted]

  val convertedSchema: StructType = ScalaReflection.schemaFor[Converted].dataType.asInstanceOf[StructType]

  val rawRecord: RawEntityStructure[Raw] = new RawEntityStructure[Raw]() {}

  val ERROR = "error"
  val SOURCE_FILE_NAME = "source_file_name"
}
