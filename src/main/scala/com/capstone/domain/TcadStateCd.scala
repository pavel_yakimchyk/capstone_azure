package com.capstone.domain

import com.capstone.sor.{ProvidedBySOR, SOR, TcadSor}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object TcadStateCd extends EntityStructure[TcadStateCdSchema] with ProvidedBySOR {

  override val schema: StructType = ScalaReflection.schemaFor[TcadStateCdSchema].dataType.asInstanceOf[StructType]

  override val tableName: String = "STATE_CD"

  override def sor: SOR = TcadSor

  val STATE_CD = "state_cd"
  val STATE_CD_DESCRIPTION = "state_cd_description"
  val PTD_STATE_CD = "ptd_state_cd"
  val PTD_STATE_CD_DESCRIPTION = "ptd_state_cd_description"
  val STATE_CD_YEAR = "state_cd_year"
}

case class TcadStateCdSchema(
                              state_cd: String,
                              state_cd_description: String,
                              ptd_state_cd: String,
                              ptd_state_cd_description: String,
                              state_cd_year: String
                            )