package com.capstone.domain

import com.capstone.sor.{ProvidedBySOR, SOR, SupplementalSor}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object StateClassHcad extends EntityStructure[StateClassHcadSchema] with ProvidedBySOR {

  override val schema: StructType = ScalaReflection.schemaFor[StateClassHcadSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = SupplementalSor

  val CODE = "code"
  val CODE2 = "code2"
  val DESCRIPTION = "description"
}

case class StateClassHcadSchema(code: String,
                                code2: String,
                                description: String)