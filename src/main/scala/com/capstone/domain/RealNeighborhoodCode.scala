package com.capstone.domain

import com.capstone.sor.{HcadSor, ProvidedBySOR, SOR}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object RealNeighborhoodCode extends EntityStructure[RealNeighborhoodCodeSchema] with ProvidedBySOR {
  override val schema: StructType = ScalaReflection.schemaFor[RealNeighborhoodCodeSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = HcadSor

  val NEIGHBORHOOD_CD = "NEIGHBORHOOD_CD"
  val GROUP_CD = "GROUP_CD"
  val DESCRIPTION = "DESCRIPTION"
  val SNAPSHOT_YEAR = "SNAPSHOT_YEAR"
}

case class RealNeighborhoodCodeSchema(
                                       NEIGHBORHOOD_CD: String,
                                       GROUP_CD: String,
                                       DESCRIPTION: String,
                                       SNAPSHOT_YEAR: String
                                     )
