package com.capstone.domain

import com.capstone.sor.{ProvidedBySOR, SOR, TcadSor}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object TcadProp extends TooWideTable[TcadPropSchema] with ProvidedBySOR {

  override val tableName: String = "PROP"

  override val schema: StructType = ScalaReflection.schemaFor[TcadPropSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = TcadSor

  val PROP_ID = addField("prop_id")
  val PROP_VAL_YR = addField("prop_val_yr")
  val PY_OWNER_ID = addField("py_owner_id")
  val PY_OWNER_NAME = addField("py_owner_name")
  val APPRAISED_VAL = addField("appraised_val")
  val ASSESSED_VAL = addField("assessed_val")
  val LAND_STATE_CD = addField("land_state_cd")
  val HOOD_CD = addField("hood_cd")
  val PY_ADDR_CITY = addField("py_addr_city")
  val PY_ADDR_ZIP = addField("py_addr_zip")
  val PY_ADDR_LINE1 = addField("py_addr_line1")
  val PY_ADDR_LINE2 = addField("py_addr_line2")
  val PY_ADDR_LINE3 = addField("py_addr_line3")
  val PY_ADDR_STATE = addField("py_addr_state")
  val PROP_TYPE_CD = addField("prop_type_cd")
  val SUP_NUM = addField("sup_num")

  import org.apache.spark.sql.functions._
  val addressIdCols = List(col(PY_ADDR_STATE), col(PY_ADDR_CITY), col(PY_ADDR_ZIP), col(PY_ADDR_LINE1))
  val neighborhoodIdCols = List(col(HOOD_CD), col(PY_ADDR_ZIP), col(PY_ADDR_LINE1))
}

case class TcadPropSchema(
                           prop_id: Long,
                           prop_val_yr: BigDecimal,
                           py_owner_id: Long,
                           py_owner_name: String,
                           appraised_val: BigDecimal,
                           assessed_val: BigDecimal,
                           land_state_cd: String,
                           hood_cd: String,
                           py_addr_city: String,
                           py_addr_zip: String,
                           py_addr_line1: String,
                           py_addr_line2: String,
                           py_addr_line3: String,
                           py_addr_state: String,
                           prop_type_cd: String,
                           sup_num: Long
                         )