package com.capstone.domain

import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

import scala.collection.mutable.ListBuffer
import scala.reflect.runtime.universe.TypeTag

trait EntityStructure[T <: Product] {

  type entitySchema = T

  val schema: StructType

  def tableName: String = {
    val outputName = "[A-Z\\d]".r.replaceAllIn(this.getClass.getSimpleName, { m => "_" + m.group(0).toLowerCase() })
    outputName.substring(1, outputName.length - 1)
  }

  private final val fields: ListBuffer[String] = ListBuffer()

  def addField(fieldName: String): String = {
    fields += fieldName
    fieldName
  }

  def getFields = fields
}

abstract class RawEntityStructure[T <: Product](implicit _rawSchema: TypeTag[T]) extends EntityStructure[T] {

  override val schema: StructType = ScalaReflection.schemaFor[T].dataType.asInstanceOf[StructType]
}
