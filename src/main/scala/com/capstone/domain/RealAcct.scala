package com.capstone.domain

import com.capstone.sor.{HcadSor, ProvidedBySOR, SOR}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object RealAcct extends EntityStructure[RealAcctSchema] with ProvidedBySOR {

  override val schema: StructType = ScalaReflection.schemaFor[RealAcctSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = HcadSor

  val ACCOUNT = "ACCOUNT"
  val TAX_YEAR = "TAX_YEAR"
  val TOTAL_APPRAISED_VALUE = "TOTAL_APPRAISED_VALUE"
  val ASSESSED_VALUE = "ASSESSED_VALUE"
  val TOTAL_MARKET_VALUE = "TOTAL_MARKET_VALUE"
  val NEIGHBORHOOD_CODE = "NEIGHBORHOOD_CODE"
  val NEIGHBORHOOD_GROUP = "NEIGHBORHOOD_GROUP"
  val SITE_ADDR_2 = "SITE_ADDR_2"
  val SITE_ADDR_3 = "SITE_ADDR_3"
  val SITE_ADDR_1 = "SITE_ADDR_1"
  val MAIL_STATE = "MAIL_STATE"
  val SNAPSHOT_YEAR = "SNAPSHOT_YEAR"

  import org.apache.spark.sql.functions._
  val addressIdCols = List(col(SITE_ADDR_1), col(SITE_ADDR_2), col(SITE_ADDR_3), col(MAIL_STATE))
  val neighborhoodIdCols = List(col(NEIGHBORHOOD_CODE), col(NEIGHBORHOOD_GROUP), col(SITE_ADDR_1))
}

case class RealAcctSchema(
                           ACCOUNT: String,
                           TAX_YEAR: String,
                           MAILTO: String,
                           MAIL_ADDR_1: String,
                           MAIL_ADDR_2: String,
                           MAIL_CITY: String,
                           MAIL_STATE: String,
                           MAIL_ZIP: String,
                           MAIL_COUNTRY: String,
                           UNDELIVERABLE: String,
                           STR_PFX: String,
                           STR_NUM: Int,
                           STR_NUM_SFX: String,
                           STR_NAME: String,
                           STR_SFX: String,
                           STR_SFX_DIR: String,
                           STR_UNIT: String,
                           SITE_ADDR_1: String,
                           SITE_ADDR_2: String,
                           SITE_ADDR_3: String,
                           STATE_CLASS: String,
                           SCHOOL_DIST: String,
                           MAP_FACET: String,
                           KEY_MAP: String,
                           NEIGHBORHOOD_CODE: String,
                           NEIGHBORHOOD_GROUP: String,
                           MARKET_AREA_1: String,
                           MARKET_AREA_1_DSCR: String,
                           MARKET_AREA_2: String,
                           MARKET_AREA_2_DSCR: String,
                           ECON_AREA: String,
                           ECON_BLD_CLASS: String,
                           CENTER_CODE: String,
                           YR_IMPR: String,
                           YR_ANNEXED: String,
                           SPLT_DT: String,
                           DSC_CD: String,
                           NXT_BUILDING: String,
                           TOTAL_BUILDING_AREA: String,
                           TOTAL_LAND_AREA: String,
                           ACREAGE: String,
                           CAP_ACCOUNT: String,
                           SHARED_CAD_CODE: String,
                           LAND_VALUE: String,
                           IMPROVEMENT_VALUE: String,
                           EXTRA_FEATURES_VALUE: String,
                           AG_VALUE: String,
                           ASSESSED_VALUE: String,
                           TOTAL_APPRAISED_VALUE: String,
                           TOTAL_MARKET_VALUE: String,
                           PRIOR_LND_VALUE: String,
                           PRIOR_IMPR_VALUE: String,
                           PRIOR_X_FEATURES_VALUE: String,
                           PRIOR_AG_VALUE: String,
                           PRIOR_TOTAL_APPRAISED_VALUE: String,
                           PRIOR_TOTAL_MARKET_VALUE: String,
                           NEW_CONSTRUCTION_VALUE: String,
                           TOTAL_RCN_VALUE: String,
                           VALUE_STATUS: String,
                           NOTICED: String,
                           NOTICE_DATE: String,
                           PROTESTED: String,
                           CERTIFIED_DATE: String,
                           LAST_INSPECTED_DATE: String,
                           LAST_INSPECTED_BY: String,
                           NEW_OWNER_DATE: String,
                           LEGAL_DSCR_1: String,
                           LEGAL_DSCR_2: String,
                           LEGAL_DSCR_3: String,
                           LEGAL_DSCR_4: String,
                           JURS: String,
                           SNAPSHOT_YEAR: String
                         )