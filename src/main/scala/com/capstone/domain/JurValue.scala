package com.capstone.domain

import com.capstone.sor.{HcadSor, ProvidedBySOR, SOR}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object JurValue extends EntityStructure[JurValueSchema] with ProvidedBySOR {

  override val schema: StructType = ScalaReflection.schemaFor[JurValueSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = HcadSor

  val ACCOUNT = "ACCOUNT"
  val TAX_DISTRICT = "TAX_DISTRICT"
  val TYPE = "TYPE"
  val PCT_DISTRICT = "PCT_DISTRICT"
  val APPRAISED_VALUE = "APPRAISED_VALUE"
  val TAXABLE_VALUE = "TAXABLE_VALUE"
  val SNAPSHOT_YEAR = "SNAPSHOT_YEAR"
}

case class JurValueSchema(ACCOUNT: String,
                          TAX_DISTRICT: String,
                          TYPE: String,
                          PCT_DISTRICT: String,
                          APPRAISED_VALUE: String,
                          TAXABLE_VALUE: String,
                          SNAPSHOT_YEAR: String
                         )