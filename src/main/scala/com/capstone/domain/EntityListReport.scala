package com.capstone.domain

import com.capstone.sor.{ProvidedBySOR, SOR, SupplementalSor}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object EntityListReport extends EntityStructure[EntityListReportSchema] with ProvidedBySOR {

  override val schema: StructType = ScalaReflection.schemaFor[EntityListReportSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = SupplementalSor

  val CODE = "code"
  val NAME = "name"
  val TYPE_DISTRICT = "type_district"
  val RATE_SUM = "rate_sum"
  val CERT = "cert"
  val PTD = "ptd"
  val APPR = "appr"
  val FZC = "fzc"
}

case class EntityListReportSchema(
                                   code: String,
                                   name: String,
                                   type_district: String,
                                   rate_sum: String,
                                   cert: String,
                                   ptd: String,
                                   appr: String,
                                   fzc: String
                                 )