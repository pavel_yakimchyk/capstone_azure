package com.capstone.domain

import com.capstone.sor.{HcadSor, ProvidedBySOR, SOR}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object JurTaxDistExemptValue extends EntityStructure [JurTaxDistExemptValueSchema] with ProvidedBySOR{
  override val schema: StructType = ScalaReflection.schemaFor[JurTaxDistExemptValueSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = HcadSor
}

case class JurTaxDistExemptValueSchema(
                                        RP_TYPE: String,
                                        TAX_DIST: String,
                                        TAX_DISTRICT_NAME: String,
                                        PRIOR_YR_RATE: String,
                                        CURRENT_YR_RATE: String,
                                        ABT: Int,
                                        APD: Int,
                                        APO: Int,
                                        APR: Int,
                                        DIS: Int,
                                        HIS: Int,
                                        LIH: Int,
                                        MCL: Int,
                                        OVR: Int,
                                        PAR: Int,
                                        PDS: Int,
                                        PEX: Int,
                                        POL: Int,
                                        POV: Int,
                                        PRO: Int,
                                        RES: Int,
                                        SOL: Int,
                                        SSA: Int,
                                        SSD: Int,
                                        STT: Int,
                                        STX: Int,
                                        SUR: Int,
                                        TOT: Int,
                                        V11: Int,
                                        V12: Int,
                                        V13: Int,
                                        V14: Int,
                                        V21: Int,
                                        V22: Int,
                                        V23: Int,
                                        V24: Int,
                                        VCH: Int,
                                        VS1: Int,
                                        VS2: Int,
                                        VS3: Int,
                                        VS4: Int,
                                        VTX: Int,
                                        ESP: Int,
                                        GCC: Int,
                                        ODR: Int,
                                        SPV: Int,
                                        UND: Int,
                                        SNAPSHOT_YEAR: String
                                      )