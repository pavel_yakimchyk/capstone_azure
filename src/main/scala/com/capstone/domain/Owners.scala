package com.capstone.domain

import com.capstone.sor.{HcadSor, ProvidedBySOR, SOR}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object Owners extends EntityStructure[OwnersSchema] with ProvidedBySOR {
  override val schema: StructType = ScalaReflection.schemaFor[OwnersSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = HcadSor

  val ACCOUNT = "ACCOUNT"
  val LINE_NUMBER = "LINE_NUMBER"
  val NAME = "NAME"
  val AKA = "AKA"
  val PCT_OWN = "PCT_OWN"
  val SNAPSHOT_YEAR = "SNAPSHOT_YEAR"
}

case class OwnersSchema(
                         ACCOUNT: String,
                         LINE_NUMBER: Int,
                         NAME: String,
                         AKA: String,
                         PCT_OWN: String,
                         SNAPSHOT_YEAR: String
                       )