package com.capstone.domain

import com.capstone.sor.{HcadSor, ProvidedBySOR, SOR}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

object BuildingRes extends EntityStructure[BuildingResSchema] with ProvidedBySOR {

  override val schema: StructType = ScalaReflection.schemaFor[BuildingResSchema].dataType.asInstanceOf[StructType]

  override def sor: SOR = HcadSor
  val ACCOUNT = "ACCOUNT"
  val USE_CODE = "USE_CODE"
}

case class BuildingResSchema(
                              ACCOUNT: String,
                              USE_CODE: String,
                              BUILDING_NUMBER: Int,
                              IMPRV_TYPE: String,
                              BUILDING_STYLE_CODE: String,
                              CLASS_STRUCTURE: String,
                              CLASS_STRUC_DESCRIPTION: String,
                              DEPRECIATION_VALUE: String,
                              CAMA_REPLACEMENT_COST: String,
                              ACCRUED_DEPR_PCT: String,
                              QUALITY: String,
                              QUALITY_DESCRIPTION: String,
                              DATE_ERECTED: String,
                              EFFECTIVE_DATE: String,
                              YR_REMODEL: String,
                              YR_ROLL: String,
                              APPRAISED_BY: String,
                              APPRAISED_DATE: String,
                              NOTE: String,
                              IMPR_SQ_FT: String,
                              ACTUAL_AREA: String,
                              HEAT_AREA: String,
                              GROSS_AREA: String,
                              EFFECTIVE_AREA: String,
                              BASE_AREA: String,
                              PERIMETER: String,
                              PERCENT_COMPLETE: String,
                              NBHD_FACTOR: String,
                              RCNLD: String,
                              SIZE_INDEX: String,
                              LUMP_SUM_ADJ: String,
                              SNAPSHOT_YEAR: String
                            )