package com.capstone.domain

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDateTime

import scala.language.implicitConversions
import scala.util.{Failure, Success, Try}

object ValWrappers {

  implicit class StringOps(s: String) {
    def toIntSafe(mandatory: Boolean = false): SafeWrapper[Int] =
      new SafeWrapper[Int](s.toInt, mandatory, 0) {}

    def toDoubleSafe(mandatory: Boolean = false): SafeWrapper[Double] =
      new SafeWrapper[Double](s.toDouble, mandatory, 0.0) {}

    def toDateSafe(mandatory: Boolean = false): SafeWrapper[Timestamp] =
      new SafeWrapper[Timestamp](parseDate(s), mandatory, Timestamp.valueOf(LocalDateTime.MIN)) {}

    def toBooleanSafe(mandatory: Boolean = false): SafeWrapper[Boolean] =
      new SafeWrapper[Boolean](s.toBoolean, mandatory, false) {}

    def toBigDecimalSafe(mandatory: Boolean = false): SafeWrapper[BigDecimal] =
      new SafeWrapper[BigDecimal](if (s == null) null else BigDecimal(s), mandatory, null) {}
  }

  implicit def unwrap[T](wrapped: SafeWrapper[T]) = wrapped.value

  private def parseDate(business_date: String) = {
    new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(business_date).getTime)
  }

  sealed abstract class SafeWrapper[T](attempt: => T, mandatory: Boolean, defaultValue: T) {
    val (error, value) = Try(attempt) match {
      case Success(res) => ("", res)
      case Failure(ex) => (ex.getMessage, if (mandatory) defaultValue else null.asInstanceOf[T])
    }
  }

}
