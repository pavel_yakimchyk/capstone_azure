package com.capstone.utils

import com.capstone.domain.EntityStructure
import org.apache.spark.sql._

object DSUtils {

  def readDS[T <: Product](entity: EntityStructure[T], path: String, ss: SparkSession)(implicit encoder: Encoder[T]): Dataset[T] = {

    println(s"Reading dataframe ${entity.tableName} from: $path:")
    val ds = DFUtils.readDF(path, ss)
      .as[entity.entitySchema]

    ds.printSchema()
    ds
  }

  def saveDsAsParquet[T <: Product](ds: Dataset[T], path: String) = {
    //log path and structure
    println(s"Saving dataset to: $path:")
    ds.printSchema()

    ds.write
      .option("compression", "uncompressed")
      .mode(SaveMode.Overwrite)
      .parquet(path)
  }

  def saveDsAsCsv[T <: Product](ds: Dataset[T], path: String) = {
    ds.write
      .mode(SaveMode.Overwrite)
      .csv(path)
  }

  def toColOrderedDS[T <: Product](df: DataFrame, entityStructure: EntityStructure[T])(implicit encoder: Encoder[T]): Dataset[T] = {
    import org.apache.spark.sql.functions._
    df
      .select(entityStructure.getFields.map(fieldName => col(fieldName)): _*)
      .as[T]
  }
}
