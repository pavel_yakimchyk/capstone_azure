package com.capstone.utils

import org.apache.spark.sql.types.{DataType, StringType}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object DFUtils {

  def readDF(path: String, ss: SparkSession): DataFrame = {
    println(s"Reading dataframe from parquet, path: $path:")
    val df = ss.read
      .parquet(path)

    println(s"SCHEMA FOR: $path:")
    df.printSchema()

    trimDF(df)
  }

  def saveDfAsParquet(df: DataFrame, path: String) = {
    println(s"Saving dataframe as parquet to: $path:")
    df.printSchema()
    df.write
      .option("compression", "uncompressed")
      .mode(SaveMode.Overwrite)
      .parquet(path)
  }

  import org.apache.spark.sql.functions._

  def trimDF(df: DataFrame) = {
    val selCols = df.schema.fields.map(x => {
      if (x.dataType == StringType) {
       trim(org.apache.spark.sql.functions.col(x.name)).as(x.name)
      } else {
        col(x.name)
      }
    })
    df.select(selCols: _*)
  }
}
