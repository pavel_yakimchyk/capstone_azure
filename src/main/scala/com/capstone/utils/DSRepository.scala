package com.capstone.utils

import com.capstone.domain.EntityStructure
import org.apache.spark.sql.{Dataset, Encoder}

import scala.collection.mutable

object DSRepository {

  val DS_MAP = new mutable.HashMap[EntityStructure[_ <: Product], Dataset[_ <: Product]]

  def putDS[T <: Product](entity: EntityStructure[T], ds: Dataset[T])(implicit encoder: Encoder[T]): Dataset[T] = {
    DS_MAP(entity) = ds
    ds
  }

  def getDS[T <: Product](entity: EntityStructure[T]): Dataset[T] = {
    DS_MAP(entity).asInstanceOf[Dataset[T]]
  }


  //    def putDS[T <: Product](entity: ConvertableEntityStructure[_, T], ds: Dataset[T]): Unit = {
  //      DS_MAP(entity) = ds
  //    }
  //
  //    def getDS[T <: Product](entity: ConvertableEntityStructure[_, T]): Dataset[T] = {
  //      DS_MAP(entity).asInstanceOf[Dataset[T]]
  //    }
}
