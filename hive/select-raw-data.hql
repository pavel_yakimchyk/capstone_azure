DROP TABLE IF EXISTS test_p1;

CREATE EXTERNAL TABLE test_p1(
    RP_TYPE string,
    TAX_DISTRICT string,
    TAX_DISTRICT_NAME string,
    PRIOR_YR_RATE string,
    CURRENT_YR_RATE string,
    ABT int,
    APD int,
    APO int,
    APR int,
    DIS int,
    HIS int,
    LIH int,
    MCL int,
    OVR int,
    PAR int,
    PDS int,
    PEX int,
    POL int,
    POV int,
    PRO int,
    RES int,
    SOL int,
    SSA int,
    SSD int,
    STT int,
    STX int,
    SUR int,
    TOT int,
    V11 int,
    V12 int,
    V13 int,
    V14 int,
    V21 int,
    V22 int,
    V23 int,
    V24 int,
    VCH int,
    VS1 int,
    VS2 int,
    VS3 int,
    VS4 int,
    VTX int,
    ESP int,
    GCC int,
    ODR int,
    SPV int,
    UND int,
    SNAPSHOT_YEAR string)
STORED AS PARQUET
LOCATION 'adl://edapadls.azuredatalakestore.net/data/raw/msq-hdinsight-team-b/HCAD/t_jur_tax_dist_exempt_value/';

DROP TABLE IF EXISTS stage_tax;
CREATE EXTERNAL TABLE stage_tax(
    TAX_DISTRICT string,
    TAX_DISTRICT_NAME string)
STORED AS PARQUET
LOCATION 'adl://edapadls.azuredatalakestore.net/data/stage/msq-hdinsight-team-b/t_jur_tax_dist_exempt_value';

INSERT OVERWRITE TABLE stage_tax
SELECT TAX_DISTRICT, TAX_DISTRICT_NAME FROM test_p1