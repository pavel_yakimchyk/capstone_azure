DROP TABLE IF EXISTS raw_capstone.jur_value;

CREATE EXTERNAL TABLE raw_capstone.jur_value(
        ACCOUNT String,
        TAX_DISTRICT string,
        TYPE string,
        PCT_DISTRICT string,
        APPRAISED_VALUE string,
        TAXABLE_VALUE string,
        SNAPSHOT_YEAR string)
        STORED AS PARQUET

LOCATION 'adl://edapadls.azuredatalakestore.net/data/raw/msq-hdinsight-team-b/HCAD/jur_value/';



DROP TABLE IF EXISTS stg_capstone.jur_value;
CREATE EXTERNAL TABLE stg_capstone.jur_value(
       ACCOUNT String,
       TAX_DISTRICT string,
       TYPE string,
       PCT_DISTRICT string,
       APPRAISED_VALUE string,
       TAXABLE_VALUE string,
       SNAPSHOT_YEAR string)
STORED AS PARQUET
LOCATION 'adl://edapadls.azuredatalakestore.net/data/stage/msq-hdinsight-team-b/HCAD/jur_value';

INSERT OVERWRITE TABLE stg_capstone.jur_value
SELECT
        ACCOUNT,
        TAX_DISTRICT,
        TYPE,
        PCT_DISTRICT,
        APPRAISED_VALUE,
        TAXABLE_VALUE,
        SNAPSHOT_YEAR
FROM raw_capstone.jur_value
;