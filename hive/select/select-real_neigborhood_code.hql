DROP TABLE IF EXISTS raw_capstone.real_neighborhood_code;

CREATE EXTERNAL TABLE raw_capstone.real_neighborhood_code(
           NEIGHBORHOOD_CD string,
           GROUP_CD string,
           DESCRIPTION string,
           SNAPSHOT_YEAR string
      )
        STORED AS PARQUET

LOCATION 'adl://edapadls.azuredatalakestore.net/data/raw/msq-hdinsight-team-b/HCAD/real_neighborhood_code/';



DROP TABLE IF EXISTS stg_capstone.real_neighborhood_code;
CREATE EXTERNAL TABLE stg_capstone.real_neighborhood_code(
           NEIGHBORHOOD_CD string,
           GROUP_CD string,
           DESCRIPTION string,
           SNAPSHOT_YEAR string
      )
STORED AS PARQUET
LOCATION 'adl://edapadls.azuredatalakestore.net/data/stage/msq-hdinsight-team-b/HCAD/real_neighborhood_code';

INSERT OVERWRITE TABLE stg_capstone.real_neighborhood_code
SELECT * FROM raw_capstone.real_neighborhood_code
;