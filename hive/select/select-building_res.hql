DROP TABLE IF EXISTS raw_capstone.building_res;

CREATE EXTERNAL TABLE raw_capstone.building_res(
             ACCOUNT string,
             USE_CODE string,
             BUILDING_NUMBER Int,
             IMPRV_TYPE string,
             BUILDING_STYLE_CODE string,
             CLASS_STRUCTURE string,
             CLASS_STRUC_DESCRIPTION string,
             DEPRECIATION_VALUE string,
             CAMA_REPLACEMENT_COST string,
             ACCRUED_DEPR_PCT string,
             QUALITY string,
             QUALITY_DESCRIPTION string,
             DATE_ERECTED string,
             EFFECTIVE_DATE string,
             YR_REMODEL string,
             YR_ROLL string,
             APPRAISED_BY string,
             APPRAISED_DATE string,
             NOTE string,
             IMPR_SQ_FT string,
             ACTUAL_AREA string,
             HEAT_AREA string,
             GROSS_AREA string,
             EFFECTIVE_AREA string,
             BASE_AREA string,
             PERIMETER string,
             PERCENT_COMPLETE string,
             NBHD_FACTOR string,
             RCNLD string,
             SIZE_INDEX string,
             LUMP_SUM_ADJ string,
             SNAPSHOT_YEAR string)
        STORED AS PARQUET

LOCATION 'adl://edapadls.azuredatalakestore.net/data/raw/msq-hdinsight-team-b/HCAD/building_res/';


DROP TABLE IF EXISTS stg_capstone.building_res;
CREATE EXTERNAL TABLE stg_capstone.building_res(
                   ACCOUNT string,
                   USE_CODE string,
                   BUILDING_NUMBER Int,
                   IMPRV_TYPE string,
                   BUILDING_STYLE_CODE string,
                   CLASS_STRUCTURE string,
                   CLASS_STRUC_DESCRIPTION string,
                   DEPRECIATION_VALUE string,
                   CAMA_REPLACEMENT_COST string,
                   ACCRUED_DEPR_PCT string,
                   QUALITY string,
                   QUALITY_DESCRIPTION string,
                   DATE_ERECTED string,
                   EFFECTIVE_DATE string,
                   YR_REMODEL string,
                   YR_ROLL string,
                   APPRAISED_BY string,
                   APPRAISED_DATE string,
                   NOTE string,
                   IMPR_SQ_FT string,
                   ACTUAL_AREA string,
                   HEAT_AREA string,
                   GROSS_AREA string,
                   EFFECTIVE_AREA string,
                   BASE_AREA string,
                   PERIMETER string,
                   PERCENT_COMPLETE string,
                   NBHD_FACTOR string,
                   RCNLD string,
                   SIZE_INDEX string,
                   LUMP_SUM_ADJ string,
                   SNAPSHOT_YEAR string)
STORED AS PARQUET
LOCATION 'adl://edapadls.azuredatalakestore.net/data/stage/msq-hdinsight-team-b/HCAD/building_res';

INSERT OVERWRITE TABLE stg_capstone.building_res
SELECT * FROM raw_capstone.building_res
;