DROP TABLE IF EXISTS raw_capstone.owners;

CREATE EXTERNAL TABLE raw_capstone.owners(
             ACCOUNT string,
             LINE_NUMBER int,
             NAME string,
             AKA string,
             PCT_OWN string,
             SNAPSHOT_YEAR string
            )
        STORED AS PARQUET

LOCATION 'adl://edapadls.azuredatalakestore.net/data/raw/msq-hdinsight-team-b/HCAD/owners/';


DROP TABLE IF EXISTS stg_capstone.owners;
CREATE EXTERNAL TABLE stg_capstone.owners(
   ACCOUNT string,
   LINE_NUMBER int,
   NAME string,
   AKA string,
   PCT_OWN string,
   SNAPSHOT_YEAR string
)
STORED AS PARQUET
LOCATION 'adl://edapadls.azuredatalakestore.net/data/stage/msq-hdinsight-team-b/HCAD/owners';

INSERT OVERWRITE TABLE stg_capstone.owners
SELECT * FROM raw_capstone.owners
;