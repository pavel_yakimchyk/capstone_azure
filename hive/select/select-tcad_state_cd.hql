DROP TABLE IF EXISTS raw_capstone.state_cd;
DROP TABLE IF EXISTS stg_capstone.state_cd;

CREATE EXTERNAL TABLE raw_capstone.state_cd (
state_cd	char(10),
state_cd_description	char(50),
ptd_state_cd	char(10),
ptd_state_cd_description	char(50),
state_cd_year	char(5)
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
    "input.regex" = "(.{10})(.{50})(.{10})(.{50})(.{4})"
)
STORED AS TEXTFILE
LOCATION '${hiveconf:raw_dir}/STATE_CD';

CREATE EXTERNAL TABLE stg_capstone.state_cd (
state_cd	char(10),
state_cd_description	char(50),
ptd_state_cd	char(10),
ptd_state_cd_description	char(50),
state_cd_year	char(5)
)
STORED AS PARQUET
LOCATION '${hiveconf:stg_dir}/STATE_CD';

INSERT OVERWRITE TABLE  stg_capstone.state_cd SELECT * FROM raw_capstone.state_cd;