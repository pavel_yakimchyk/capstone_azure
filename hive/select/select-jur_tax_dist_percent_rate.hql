DROP TABLE IF EXISTS raw_capstone.jur_tax_dist_percent_rate;

CREATE EXTERNAL TABLE raw_capstone.jur_tax_dist_percent_rate(
    RP_TYPE STRING,
	TAX_DIST STRING,
	TAX_DISTRICT_NAME STRING,
	PRIOR_YR_RATE STRING,
	CURRENT_YR_RATE STRING,
	ABT STRING,
	APD STRING,
	APO STRING,
	APR STRING,
	DIS STRING,
	HIS STRING,
	LIH STRING,
	MCL STRING,
	OVR STRING,
	PAR STRING,
	PDS STRING,
	PEX STRING,
	POL STRING,
	POV STRING,
	PRO STRING,
	RES STRING,
	SOL STRING,
	SSA STRING,
	SSD STRING,
	STT STRING,
	STX STRING,
	SUR STRING,
	TOT STRING,
	V11 STRING,
	V12 STRING,
	V13 STRING,
	V14 STRING,
	V21 STRING,
	V22 STRING,
	V23 STRING,
	V24 STRING,
	VCH STRING,
	VS1 STRING,
	VS2 STRING,
	VS3 STRING,
	VS4 STRING,
	VTX STRING,
	ESP STRING,
	GCC STRING,
	ODR STRING,
	SPV STRING,
	UND STRING,
    SNAPSHOT_YEAR STRING
   )
STORED AS PARQUET
LOCATION 'adl://edapadls.azuredatalakestore.net/data/raw/msq-hdinsight-team-b/HCAD/jur_tax_dist_percent_rate/';

--copy data to stage

DROP TABLE IF EXISTS stg_capstone.jur_tax_dist_percent_rate;

CREATE EXTERNAL TABLE stg_capstone.jur_tax_dist_percent_rate(
        RP_TYPE STRING,
     	TAX_DIST STRING,
     	TAX_DISTRICT_NAME STRING,
     	PRIOR_YR_RATE STRING,
     	CURRENT_YR_RATE STRING,
     	ABT STRING,
     	APD STRING,
     	APO STRING,
     	APR STRING,
     	DIS STRING,
     	HIS STRING,
     	LIH STRING,
     	MCL STRING,
     	OVR STRING,
     	PAR STRING,
     	PDS STRING,
     	PEX STRING,
     	POL STRING,
     	POV STRING,
     	PRO STRING,
     	RES STRING,
     	SOL STRING,
     	SSA STRING,
     	SSD STRING,
     	STT STRING,
     	STX STRING,
     	SUR STRING,
     	TOT STRING,
     	V11 STRING,
     	V12 STRING,
     	V13 STRING,
     	V14 STRING,
     	V21 STRING,
     	V22 STRING,
     	V23 STRING,
     	V24 STRING,
     	VCH STRING,
     	VS1 STRING,
     	VS2 STRING,
     	VS3 STRING,
     	VS4 STRING,
     	VTX STRING,
     	ESP STRING,
     	GCC STRING,
     	ODR STRING,
     	SPV STRING,
     	UND STRING,
        SNAPSHOT_YEAR STRING
   )
STORED AS PARQUET
LOCATION 'adl://edapadls.azuredatalakestore.net/data/stage/msq-hdinsight-team-b/HCAD/jur_tax_dist_percent_rate';

INSERT OVERWRITE TABLE stg_capstone.jur_tax_dist_percent_rate
SELECT * FROM raw_capstone.jur_tax_dist_percent_rate
;