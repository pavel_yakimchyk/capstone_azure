$resourceGroupName = "msq-hdinsight-team-b"
$dataFactoryName = "team-b-datafactory"
$selectHCADPipelineName = "select-HCAD"
$selectTCADPipelineName = "select-TCAD"
$selectSupplementalPipelineName = "select-supplemental"

$selectHCADPipelineFilePath = "../pipelines/selectors/select-HCAD.json"
$selectTCADPipelineFilePath = "../pipelines/selectors/select-TCAD.json"
$selectSupplementalPipelineFilePath = "../pipelines/selectors/select-supplemental.json"

Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $selectHCADPipelineName -File $selectHCADPipelineFilePath
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $selectTCADPipelineName -File $selectTCADPipelineFilePath
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $selectSupplementalPipelineName -File $selectSupplementalPipelineFilePath