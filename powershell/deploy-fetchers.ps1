$resourceGroupName = "msq-hdinsight-te
$dataFactoryName = "team-b-datafactory
$fetchHCADPipelineName = "fetch-HCAD"
$fetchTCADPipelineName = "fetch-TCAD"

$fetchHCADPipelineFilePath = "../pipelines/fetchers/fetch-HCAD.json"
$fetchTCADPipelineFilePath = "../pipelines/fetchers/fetch-TCAD.json"
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $fetchHCADPipelineName -File $fetchHCADPipelineFilePath
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $fetchTCADPipelineName -File $fetchTCADPipelineFilePath