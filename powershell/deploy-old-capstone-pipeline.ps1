$resourceGroupName = "msq-hdinsight-team-b"
$dataFactoryName = "team-b-datafactory"
$pipelineName = "CapstonePipeline"
$pipelineFilePath = "../json/CapstonePipeline.json"
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $pipelineName -File $pipelineFilePath