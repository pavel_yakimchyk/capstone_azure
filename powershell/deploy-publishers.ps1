$resourceGroupName = "msq-hdinsight-team-b"
$dataFactoryName = "team-b-datafactory"
$publishPipelineName = "publish-data"
$publishPipelineFilePath = "../pipelines/publishers/publish-data.json"

Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $publishPipelineName -File $publishPipelineFilePath
