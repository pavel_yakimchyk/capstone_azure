$resourceGroupName = "msq-hdinsight-team-b"
$dataFactoryName = "team-b-datafactory"

$fetchHCADPipelineName = "fetch-HCAD"
$fetchTCADPipelineName = "fetch-TCAD"
$selectHCADPipelineName = "select-HCAD"
$selectTCADPipelineName = "select-TCAD"
$producePipelineName = "produce-data"
$publishPipelineName = "publish-data"
$fullrunPipelineName = "FullRun"

$fetchHCADPipelineFilePath = "../pipelines/fetchers/fetch-HCAD.json"
$fetchTCADPipelineFilePath = "../pipelines/fetchers/fetch-TCAD.json"
$selectHCADPipelineFilePath = "../pipelines/selectors/select-HCAD.json"
$selectTCADPipelineFilePath = "../pipelines/selectors/select-TCAD.json"
$producePipelineFilePath = "../pipelines/producers/produce-data.json"
$publishPipelineFilePath = "../pipelines/publishers/publish-data.json"
$fullrunPipelineFilePath = "../pipelines/FullRunPipeline.json"

Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $fetchHCADPipelineName -File $fetchHCADPipelineFilePath
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $fetchTCADPipelineName -File $fetchTCADPipelineFilePath
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $selectHCADPipelineName -File $selectHCADPipelineFilePath
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $selectTCADPipelineName -File $selectTCADPipelineFilePath
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $producePipelineName -File $producePipelineFilePath
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $publishPipelineName -File $publishPipelineFilePath
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $fullrunPipelineName -File $fullrunPipelineFilePath