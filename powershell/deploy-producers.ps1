$resourceGroupName = "msq-hdinsight-team-b"
$dataFactoryName = "team-b-datafactory"
$producePipelineName = "produce-data"
$producePipelineFilePath = "../pipelines/producers/produce-data.json"
Set-AzureRmDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $producePipelineName -File $producePipelineFilePath
